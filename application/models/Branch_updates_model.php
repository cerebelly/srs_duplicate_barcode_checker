<?php 
defined('BASEPATH') OR exit('No direct script access allowed!');

class Branch_updates_model extends CI_Model {

	public function get_main_updates($branchConn, $branchCode, $dateFrom, $dateTo) {
		load_db($branchConn);
		$sql = "select 
				 MainBarcode.MThrow_0 as INCOMPLETE,
				 MainBarcode.Mbranch_code as Incomplete_Branch,
				 BranchBarcode.BThrow_1 as COMPLETED,
				 BranchBarcode.Bbranch_code as Branch_Completed

				 from
				 (
				select Throw_0 as MThrow_0,f_branch_code as Mbranch_code
				from 
				(SELECT
				count(id)  as Throw_0,
				p.branch_code as f_branch_code
				FROM branch_updates as p where throw =0 and  branch_code = '".$branchCode."' and  CAST(date_added as date )  between '".dateformat($dateFrom)."' and '".dateformat($dateTo)."'
				group by p.branch_code

				)  as a

				) as MainBarcode
				full join

				(

				select Throw_1 as BThrow_1,f_branch_code as Bbranch_code
				from 
				(SELECT
				count(id)  as Throw_1,
				p.branch_code as f_branch_code
				FROM branch_updates as p where throw =1 and  branch_code = '".$branchCode."' and  CAST(date_added as date )  between '".dateformat($dateFrom)."' and '".dateformat($dateTo)."'
				group by p.branch_code 
				)  as a

				) as BranchBarcode

				on  MainBarcode.Mbranch_code=BranchBarcode.Bbranch_code   or   BranchBarcode.Bbranch_code=MainBarcode.Mbranch_code";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}

}