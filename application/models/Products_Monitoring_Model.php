<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed!');

class Products_monitoring_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function get_price_change($date_from, $date_to, $priceMode, $db_name) {
		load_db($db_name);
		$sql = "SELECT 
				IncreaseDecrease = CASE WHEN r2.tosrp > r2.fromsrp THEN 1 WHEN r2.tosrp < r2.fromsrp THEN 0 END,
				r2.*, products.description AS mdescription, 
				pricemode.description AS mPriceMode 
				FROM 
				(SELECT pricechangehistory.* 
				    FROM (SELECT productid, barcode, pricemodecode, dateposted, uom, MAX(lineid) AS lineid 
				                FROM pricechangehistory 
				                WHERE datediff(day, dateposted, '".$date_from."') <=0 
				                AND datediff(day,dateposted, '".$date_to."') >= 0";

				                if (isset($priceMode) && !empty($priceMode)) {
				                	$sql .= "AND pricemodecode LIKE '%".$priceMode."%'";
				                }

				         $sql .= "GROUP BY productid,barcode,pricemodecode,dateposted,uom) AS r1 
				LEFT JOIN pricechangehistory ON pricechangehistory.lineid = r1.lineid) AS r2 
				LEFT JOIN pricemode ON pricemode.pricemodecode = r2.pricemodecode 
				LEFT JOIN products ON products.productid = r2.productid 
				LEFT JOIN pos_products ON pos_products.productid = r2.productid 
				AND pos_products.barcode = r2.barcode 
				AND pos_products.pricemodecode = r2.pricemodecode 
				WHERE r2.tosrp <> r2.fromsrp 
				ORDER BY products.description";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		if ($query->num_rows() > 0) {
			return $result;
		}
	}

}