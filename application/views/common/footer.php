  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 0.0.1
    </div>
    <strong>Copyright &copy; 2016-2017 <a href="#">San Roque Supermarket</a>.</strong> All rights
    reserved.
  </footer>

</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url('assets'); ?>/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url('assets'); ?>/bootstrap/js/bootstrap.min.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url('assets'); ?>/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- PACE -->
<script src="<?php echo base_url('assets'); ?>/plugins/pace/pace.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('assets'); ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets'); ?>/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets'); ?>/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assets'); ?>/dist/js/demo.js"></script>
<!-- Accounting JS -->
<script src="<?php echo base_url('assets'); ?>/public/javascripts/accounting.min.js"></script>
<!-- Select2 JS -->
<script src="<?php echo base_url('assets'); ?>/plugins/select2/select2.full.min.js"></script>
<!-- page script -->
<script type="text/javascript">
	// To make Pace works on Ajax calls
	$(document).ajaxStart(function() { Pace.restart(); });
    $('.ajax').click(function(){
        $.ajax({url: '#', success: function(result){
            $('.ajax-content').html('<hr>Ajax Request Completed !');
        }});
    });

  <?php if (current_url() == base_url('index.php/Products_monitoring/price_change')) { ?>
  $(document).ready(function() {
      // Date Picker
      $('#dateFrom').datepicker({
        autoclose: true
      });

      $('#dateTo').datepicker({
        autoclose: true
      });
      // End Date Picker

      // Accounting.js Settings
      accounting.settings = {
        currency: {
          symbol : "&#8369;",   // default currency symbol is '$'; changed into '₱' HTML Entity (decimal)
          format: "%s%v", // controls output: %s = symbol, %v = value/number (can be object: see below)
          decimal : ".",  // decimal point separator
          thousand: ",",  // thousands separator
          precision : 2   // decimal places
        },
        number: {
          precision : 0,  // default precision on numbers is 0
          thousand: ",",
          decimal : "."
        }
      }
      // End Accounting.js

      // Get Price Change History By Branch
      $(document).on('click', '#search', function() {
        if ($('#branch').val() != '') {
          var currentRequest = null;
          if ($('#products_section_many').is(':visible')) {
            $('#products_section_many').hide();
          }

          if (!$('#products_section').is(':visible')) {
            $('#products_section').show();
          }

          if ($('#export').is(':visible')) {
            $('#export').hide();
          }

          $('#products_container').find("tr:gt(0)").remove();
          var loader = '<tr id="loader" style="text-align: center; font-size: 10px;">'
                      +'<td colspan="8">'
                        +'<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>'
                      +'</td>'
                    +'</tr>';
          $(loader).appendTo($('#products_container'));

          currentRequest = $.ajax({
            type: 'POST',
            url: '<?php echo base_url('Products_monitoring/get_products'); ?>',
            data: {
              dateFrom: $('#dateFrom').val(),
              dateTo: $('#dateTo').val(),
              priceMode: $('#priceMode').val(),
              branch: $('#branch').val()
            },
            dataType: 'json',
            beforeSend: function(xhr, opts) {
                if (currentRequest != null) {
                  currentRequest.abort();
                  return false;
                }
              }
          }).done(function(response) {
            console.log(response);
            if (response['status'] == 'NO_RESULT') {
              $('#loader').remove();

              $('#export').attr('data-dateFrom', '');
              $('#export').attr('data-dateTo', '');
              $('#export').attr('data-priceMode', '');
              $('#export').attr('data-branch', '');
              $('#export').hide();

              var loader = '<tr id="loader" style="text-align: center; font-size: 10px;">'
                      +'<td colspan="8">'
                        +'<h3>No Result!</h3>'
                      +'</td>'
                    +'</tr>';
              $(loader).appendTo($('#products_container'));
              $('#branch_name').text(response['branch_name']+' - Price Change History');
            }
            else {
              $('#loader').remove();
              $('#branch_name').text(response['branch_name']+' - Price Change History');

              $('#export').attr('data-dateFrom', $('#dateFrom').val());
              $('#export').attr('data-dateTo', $('#dateTo').val());
              $('#export').attr('data-priceMode', $('#priceMode').val());
              $('#export').attr('data-branch', $('#branch').val());
              $('#export').show();

              $.each(response['products'], function(i, value) {
                var TrData = '<tr>'
                              +'<td>'+value['barcode']+'</td>'
                              +'<td>'+value['mdescription']+'</td>'
                              +'<td>'+value['UOM']+'</td>';

                              if (value['PriceModecode'] == 'R') {
                                TrData += '<td>RETAIL</td>';
                              }
                              else {
                                TrData += '<td>WHOLESALE</td>';
                              }
                              
                              TrData += '<td>'+accounting.formatMoney(value['fromsrp'])+'</td>'
                                        +'<td>'+accounting.formatMoney(value['tosrp'])+'</td>'
                                        +'<td>'+value['dateposted']+'</td>';

                              if (value['IncreaseDecrease'] == 1) {
                                TrData += '<td><span class="label label-success">Increased</span></td>';
                              }
                              else {
                                TrData += '<td><span class="label label-warning">Decreased</span></td>';
                              }
                              
                            TrData += '</tr>';
                $(TrData).appendTo('#products_container');
              });
            }
          });
        }
        else {
          <?php 
            if (isset($branches)) {
              foreach ($branches as $key => $value) {
          ?>
            $('.branches-btn').removeClass('bg-navy');
            $('.branches-btn').addClass('btn-default');
            window.location.hash = "#ms_novaliches";

            var currentRequest<?php echo (isset($key) ? $key : ''); ?> = null;
            if ($('#products_section').is(':visible')) {
              $('#products_section').hide();
            }

            if (!$('#products_section_many').is(':visible')) {
              $('#products_section_many').show();
            }

            if ($('#export<?php echo (isset($key)) ? $key : ''; ?>').is(':visible')) {
              $('#export<?php echo (isset($key)) ? $key : ''; ?>').hide();
            }

            $('#products_container<?php echo (isset($key)) ? $key : ''; ?>').find("tr:gt(0)").remove();
            var loader = '<tr id="loader<?php echo (isset($key)) ? $key : ''; ?>" style="text-align: center; font-size: 10px;">'
                        +'<td colspan="8">'
                          +'<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>'
                        +'</td>'
                      +'</tr>';
            $(loader).appendTo($('#products_container<?php echo (isset($key)) ? $key : ''; ?>'));

            currentRequest<?php echo (isset($key) ? $key : ''); ?> = $.ajax({
              type: 'POST',
              url: '<?php echo base_url('Products_monitoring/get_products'); ?>',
              data: {
                dateFrom: $('#dateFrom').val(),
                dateTo: $('#dateTo').val(),
                priceMode: $('#priceMode').val(),
                branch: '<?php echo (isset($key)) ? $key : ''; ?>'
              },
              dataType: 'json',
              beforeSend: function(xhr, opts) {
                if (currentRequest<?php echo (isset($key) ? $key : ''); ?> != null) {
                  xhr.abort();
                }
              }
            }).done(function(response) {
              console.log(response);
              if (response['status'] == 'NO_RESULT') {
                $('#loader<?php echo (isset($key)) ? $key : ''; ?>').remove();

                $('#export<?php echo (isset($key)) ? $key : ''; ?>').attr('data-dateFrom', '');
                $('#export<?php echo (isset($key)) ? $key : ''; ?>').attr('data-dateTo', '');
                $('#export<?php echo (isset($key)) ? $key : ''; ?>').attr('data-priceMode', '');
                $('#export<?php echo (isset($key)) ? $key : ''; ?>').attr('data-branch', '');
                $('#export<?php echo (isset($key)) ? $key : ''; ?>').hide();

                var loader = '<tr id="loader<?php echo (isset($key)) ? $key : ''; ?>" style="text-align: center; font-size: 10px;">'
                        +'<td colspan="8">'
                          +'<h3>No Result!</h3>'
                        +'</td>'
                      +'</tr>';
                $(loader).appendTo($('#products_container<?php echo (isset($key)) ? $key : ''; ?>'));
                $('#branch_name<?php echo (isset($key)) ? $key : ''; ?>').text(response['branch_name']+' - Price Change History');
              }
              else {
                $('#loader<?php echo (isset($key)) ? $key : ''; ?>').remove();
                $('#branch_name<?php echo (isset($key)) ? $key : ''; ?>').text(response['branch_name']+' - Price Change History');

                $('#export<?php echo (isset($key)) ? $key : ''; ?>').attr('data-dateFrom', $('#dateFrom').val());
                $('#export<?php echo (isset($key)) ? $key : ''; ?>').attr('data-dateTo', $('#dateTo').val());
                $('#export<?php echo (isset($key)) ? $key : ''; ?>').attr('data-priceMode', $('#priceMode').val());
                $('#export<?php echo (isset($key)) ? $key : ''; ?>').attr('data-branch', $('#branch').val());
                $('#export<?php echo (isset($key)) ? $key : ''; ?>').show();

                $.each(response['products'], function(i, value) {
                  var TrData = '<tr>'
                                +'<td>'+value['barcode']+'</td>'
                                +'<td>'+value['mdescription']+'</td>'
                                +'<td>'+value['UOM']+'</td>';

                                if (value['PriceModecode'] == 'R') {
                                  TrData += '<td>RETAIL</td>';
                                }
                                else {
                                  TrData += '<td>WHOLESALE</td>';
                                }
                                
                                TrData += '<td>'+accounting.formatMoney(value['fromsrp'])+'</td>'
                                          +'<td>'+accounting.formatMoney(value['tosrp'])+'</td>'
                                          +'<td>'+value['dateposted']+'</td>';

                                if (value['IncreaseDecrease'] == 1) {
                                  TrData += '<td><span class="label label-success">Increased</span></td>';
                                }
                                else {
                                  TrData += '<td><span class="label label-warning">Decreased</span></td>';
                                }
                                
                              TrData += '</tr>';
                  $(TrData).appendTo('#products_container<?php echo (isset($key)) ? $key : ''; ?>');
                });
              }
            });

            // Export as Excel Many
            $(document).on('click', '#export<?php echo (isset($key)) ? $key : ''; ?>', function(e) {
              e.preventDefault(); 
              var dateFrom = $(this).attr('data-dateFrom');
              var dateTo = $(this).attr('data-dateTo');
              var priceMode = $(this).attr('data-priceMode');
              var branch = '<?php echo (isset($key)) ? $key : ''; ?>';
              location.href = '<?php echo base_url('Products_monitoring/export_to_excel'); ?>'+'?dateFrom='+dateFrom+'&dateTo='+dateTo+'&priceMode='+priceMode+'&branch='+branch;
            });
            // End Export Many
          <?php
              }
            }
          ?>
        }
      });
      // End 

      // Export as Excel
      $(document).on('click', '#export', function(e) {
        e.preventDefault(); 
        var dateFrom = $(this).attr('data-dateFrom');
        var dateTo = $(this).attr('data-dateTo');
        var priceMode = $(this).attr('data-priceMode');
        var branch = $(this).attr('data-branch');
        location.href = '<?php echo base_url('Products_monitoring/export_to_excel'); ?>'+'?dateFrom='+dateFrom+'&dateTo='+dateTo+'&priceMode='+priceMode+'&branch='+branch;
      });
      // End Export

      $(document).on('click', '.branches-btn', function(e) {
        $('.branches-btn').removeClass('bg-navy');
        $('.branches-btn').addClass('btn-default');
        $(e.target).removeClass('btn-default');
        $(e.target).addClass('bg-navy');  
      });
      
  });
  <?php } ?>

  <?php if (current_url() == base_url('index.php/Branch_updates')) { ?>
  $(document).ready(function() {
      // Date Picker
      $('#dateFrom').datepicker({
        autoclose: true
      });

      $('#dateTo').datepicker({
        autoclose: true
      });
      // End Date Picker

      $(document).on('click', '#check', function() {
        
        <?php
          if (isset($branches) && !empty($branches)) {
            foreach ($branches as $key => $value) {
        ?>
          $('#main_updates_table_<?php echo $key; ?>').find("tr:gt(0)").remove();
          var loader = '<tr id="msg_<?php echo $key; ?>" style="text-align: center; font-size: 10px;">'
                      +'<td colspan="8">'
                        +'<span class="label label-success" style="font-size: 10px;">Connecting to main ... <i class="fa fa-refresh fa-pulse fa-fw"></i></span>'
                      +'</td>'
                    +'</tr>';
          $(loader).appendTo($('#main_updates_table_<?php echo $key; ?>'));

          $.ajax({
            url: '<?php echo base_url('Branch_updates/get_main_updates'); ?>',
            data: {
              dateFrom: $('#dateFrom').val(),
              dateTo: $('#dateTo').val(),
              branchConn: '<?php echo $key ?>'
            },
            type: 'POST',
            dataType: 'json'
          }).done(function(response) {
            if (response['status'] == 'INVALID_CREDENTIALS') {
              $('#msg_<?php echo $key; ?>').remove();
              var newTR = '<tr id="msg_<?php echo $key; ?>" style="text-align: center; font-size: 10px;">'
                      +'<td colspan="8">'
                        +'<span class="label label-warning" style="font-size: 10px;">Invalid Credentials, Please Try Again! <i class="fa fa-exclamation-triangle"></i></span>'
                      +'</td>'
                    +'</tr>';
              $(newTR).appendTo($('#main_updates_table_<?php echo $key; ?>'));
            }
            else if (response['status'] == 'FAILED_TO_CONNECT') {
              $('#msg_<?php echo $key; ?>').remove();
              var newTR = '<tr id="msg_<?php echo $key; ?>" style="text-align: center; font-size: 10px;">'
                      +'<td colspan="8">'
                        +'<span class="label label-danger" style="font-size: 10px;">FAILED TO CONNECT: OFFLINE! <i class="fa fa-times-circle-o"></i></span>'
                      +'</td>'
                    +'</tr>';
              $(newTR).appendTo($('#main_updates_table_<?php echo $key; ?>'));
            }
            else if (response['status'] == 'HAS_RESULT') {
              $('#msg_<?php echo $key; ?>').remove();
              var newTR = '<tr>';
                      
              if (response['result'][0]['INCOMPLETE']) {
                newTR += '<td style="background-color: #dd4b39;">'+response['result'][0]['INCOMPLETE']+'</td>';
              }
              else {
                newTR += '<td>'+response['result'][0]['INCOMPLETE']+'</td>';
              }


              if (response['result'][0]['Incomplete_Branch']) {
                newTR += '<td style="background-color: #dd4b39;">'+response['result'][0]['Incomplete_Branch']+'</td>';
              }
              else {
                newTR += '<td>'+response['result'][0]['Incomplete_Branch']+'</td>';
              }
                            
              newTR += '<td>'+response['result'][0]['COMPLETED']+'</td>'
                +'<td>'+response['result'][0]['Branch_Completed']+'</td>'
                +'</tr>';
              $(newTR).appendTo($('#main_updates_table_<?php echo $key; ?>'));
            }
          });

          $('#branch_updates_table_<?php echo $key; ?>').find("tr:gt(0)").remove();
          var loader = '<tr id="bmsg_<?php echo $key; ?>" style="text-align: center; font-size: 10px;">'
                      +'<td colspan="8">'
                        +'<span class="label label-success" style="font-size: 10px;">Connecting to branch ... <i class="fa fa-refresh fa-pulse fa-fw"></i></span>'
                      +'</td>'
                    +'</tr>';
          $(loader).appendTo($('#branch_updates_table_<?php echo $key; ?>'));

          $.ajax({
            url: '<?php echo base_url('Branch_updates/get_branch_updates'); ?>',
            data: {
              dateFrom: $('#dateFrom').val(),
              dateTo: $('#dateTo').val(),
              branchConn: '<?php echo $key ?>'
            },
            type: 'POST',
            dataType: 'json'
          }).done(function(response) {
            if (response['status'] == 'INVALID_CREDENTIALS') {
              $('#msg_<?php echo $key; ?>').remove();
              var newTR = '<tr id="bmsg_<?php echo $key; ?>" style="text-align: center; font-size: 10px;">'
                      +'<td colspan="8">'
                        +'<span class="label label-warning" style="font-size: 10px;">Invalid Credentials, Please Try Again! <i class="fa fa-exclamation-triangle"></i></span>'
                      +'</td>'
                    +'</tr>';
              $(newTR).appendTo($('#branch_updates_table_<?php echo $key; ?>'));
            }
            else if (response['status'] == 'FAILED_TO_CONNECT') {
              $('#bmsg_<?php echo $key; ?>').remove();
              var newTR = '<tr id="bmsg_<?php echo $key; ?>" style="text-align: center; font-size: 10px;">'
                      +'<td colspan="8">'
                        +'<span class="label label-danger" style="font-size: 10px;">FAILED TO CONNECT: OFFLINE! <i class="fa fa-times-circle-o"></i></span>'
                      +'</td>'
                    +'</tr>';
              $(newTR).appendTo($('#branch_updates_table_<?php echo $key; ?>'));
            }
            else if (response['status'] == 'HAS_RESULT') {
              $('#bmsg_<?php echo $key; ?>').remove();
              var newTR = '<tr>';
                      
              if (response['result'][0]['INCOMPLETE']) {
                newTR += '<td style="background-color: #dd4b39;">'+response['result'][0]['INCOMPLETE']+'</td>';
              }
              else {
                newTR += '<td>'+response['result'][0]['INCOMPLETE']+'</td>';
              }


              if (response['result'][0]['Incomplete_Branch']) {
                newTR += '<td style="background-color: #dd4b39;">'+response['result'][0]['Incomplete_Branch']+'</td>';
              }
              else {
                newTR += '<td>'+response['result'][0]['Incomplete_Branch']+'</td>';
              }
                            
              newTR += '<td>'+response['result'][0]['COMPLETED']+'</td>'
                +'<td>'+response['result'][0]['Branch_Completed']+'</td>'
                +'</tr>';
              $(newTR).appendTo($('#branch_updates_table_<?php echo $key; ?>'));
            }
          });
        <?php          
            }  
          }
        ?>

      });

      $(document).on('click', '.branches-btn', function(e) {
        $('.branches-btn').removeClass('bg-navy');
        $('.branches-btn').addClass('btn-default');
        $(e.target).removeClass('btn-default');
        $(e.target).addClass('bg-navy');  
      });

  });
  <?php } ?>
</script>
</body>
</html>
