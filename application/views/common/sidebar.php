  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">NAVIGATIOM</li>
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-circle-o text-red"></i> <span>Duplicate Barcode</span></a></li>
        <li><a href="<?php echo base_url('Products_monitoring/price_change'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Price Change</span></a></li>
        <li><a href="<?php echo base_url('Branch_updates'); ?>"><i class="fa fa-circle-o text-aqua"></i> <span>Branch Updates</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->