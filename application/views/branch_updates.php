  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo (isset($header['title']) ? $header['title'] : ''); ?>
        <small><?php echo (isset($header['desc']) ? $header['desc'] : ''); ?></small>
      </h1>
    </section>

    <section class="content" style="min-height: 150px;">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">

            <div class="box-body">
              <form action="#">
                  <div class="col-md-offset-3 col-md-2">
                    <div class="form-group">
                      <label>From</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="dateFrom" id="dateFrom" class="form-control pull-right" value="<?php echo dateformat(today(), 'm/d/Y'); ?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                      <label>To</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="dateTo" id="dateTo" class="form-control pull-right" value="<?php echo dateformat(today(), 'm/d/Y'); ?>">
                      </div>
                    </div>
                  </div>
                  
                  <div class="col-md-2">
                    <div class="form-group">
                     <label class="control-label"></label>
                      <input type="button" id="check" class="btn btn-danger form-control" value="Check" style="margin-top: 5px;">
                    </div>  
                  </div>
              </form>
            </div>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-2">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Branches</h3>
            </div>
            <div class="box-body">
            <?php 
              if (isset($branches)) {
                foreach ($branches as $key => $value) {
                  echo '<a href="#'.$key.'" class="btn btn-default btn-block btn-sm branches-btn">'.$value['branch_name'].'</a>';
                }
              }
            ?>
            </div>
          </div>
        </div>
        
        <div class="col-md-10" style="height: 800px; overflow-y: scroll;">
        <?php
          if (isset($branches)) {
            foreach ($branches as $key => $value) {    
        ?>
          <div id="<?php echo (isset($key) ? $key : ''); ?>" class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header with-border">
                  <h3 id="branch_name_<?php echo (isset($key)) ? $key : ''; ?>" class="box-title">
                    <?php echo isset($value['branch_name']) ? $value['branch_name'] : ''; ?>
                  </h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <p><b>Main<b></p>
                  <table id="main_updates_table_<?php echo (isset($key)) ? $key : ''; ?>" class="table table-bordered">
                    <thead>
                      <tr>
                        <th>Incomplete</th>
                        <th>Incomplete Branch</th>
                        <th>Completed</th>
                        <th>Branch Completed</th>
                      </tr>  
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
                <!-- /.box-body -->
                <div class="box-body">
                  <p><b>Branch</b></p>
                  <table id="branch_updates_table_<?php echo (isset($key)) ? $key : ''; ?>" class="table table-bordered">
                    <thead>
                      <tr>
                        <th>Incomplete</th>
                        <th>Incomplete Branch</th>
                        <th>Completed</th>
                        <th>Branch Completed</th>
                      </tr>  
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        <?php 
          }
        }
        ?>
        </div>
      </div>
    </section>

  </div>
  <!-- /.content-wrapper -->
