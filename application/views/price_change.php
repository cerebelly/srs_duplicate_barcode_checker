  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo (isset($header['title']) ? $header['title'] : ''); ?>
        <small><?php echo (isset($header['desc']) ? $header['desc'] : ''); ?></small>
      </h1>
    </section>

    <section class="content" style="min-height: 150px;">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">

            <div class="box-body">
              <form action="#">
                  <div class="col-md-offset-1 col-md-2">
                    <div class="form-group">
                      <label>From</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="dateFrom" id="dateFrom" class="form-control pull-right" value="<?php echo dateformat(today(), 'm/d/Y'); ?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                      <label>To</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="dateTo" id="dateTo" class="form-control pull-right" value="<?php echo dateformat(today(), 'm/d/Y'); ?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                      <label>Price Mode</label>
                      <select id="priceMode" name="priceMode" class="form-control">
                        <?php 
                          if (isset($priceModes)) {
                            $selected = '';
                            echo '<option value="">All Mode</option>';
                            foreach ($priceModes as $key => $value) {
                              if (isset($priceMode)) {
                                if ($priceMode == $key) {
                                  $selected = 'selected="selected"';}
                                else {
                                  $selected = '';
                                }
                              }
                              
                              echo '<option value="'.$key.'" '.$selected.'>'.$value.'</option>';
                            }
                          } 
                          else {
                            echo "<option>No Price Modes Loaded!</option>";
                          }

                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                      <label class="control-label">Select Branch</label>
                      <select id="branch" name="branch" class="form-control">
                        <?php 
                          if (isset($branches)) {
                            $selected = '';
                            echo '<option value="">All Branch</option>';
                            foreach ($branches as $key => $value) {
                              if (isset($branch)) {
                                if ($branch == $key) {
                                  $selected = 'selected="selected"';}
                                else {
                                  $selected = '';
                                }
                              }
                              
                              echo '<option value="'.$key.'" '.$selected.'>'.$value.'</option>';
                            }
                          } 
                          else {
                            echo "<option>No Branches Loaded!</option>";
                          }

                        ?>
                      </select>
                    </div>
                  </div>
                  
                  <div class="col-md-2">
                    <div class="form-group">
                     <label class="control-label"></label>
                      <input type="button" id="search" class="btn btn-danger form-control" value="Search" style="margin-top: 5px;">
                    </div>  
                  </div>
              </form>
            </div>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>

    <section id="products_section_many" class="content" style="display: none;">
      <div class="row">
        <div class="col-md-2">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Branches</h3>
            </div>
            <div class="box-body">
            <?php 
              if (isset($branches)) {
                foreach ($branches as $key => $value) {
                  echo '<a href="#'.$key.'" class="btn btn-default btn-block btn-sm branches-btn">'.$value.'</a>';
                }
              }
            ?>
            </div>
          </div>
        </div>
        <div class="col-md-10" style="height: 800px; overflow-y: scroll;">
        <?php
          if (isset($branches)) {
            foreach ($branches as $key => $value) {    
        ?>
          <div id="<?php echo (isset($key) ? $key : ''); ?>" class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header with-border">
                  <h3 id="branch_name<?php echo (isset($key)) ? $key : ''; ?>" class="box-title"></h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <button id="export<?php echo (isset($key)) ? $key : ''; ?>" type="button" class="btn btn-default pull-right" data-dateFrom="" data-dateTo="" data-priceMode="" data-branch="" style="display: none; margin-bottom: 10px;">
                    <span class="fa fa-file-excel-o pull-left" style="font-size: 18px;"></span> Export As Excel
                  </button>
                  <table id="products_container<?php echo (isset($key)) ? $key : ''; ?>" class="table table-bordered">
                    <thead>
                      <tr>
                        <th>Barcode</th>
                        <th>Description</th>
                        <th>UOM</th>
                        <th>Price Mode</th>
                        <th>From SRP</th>
                        <th>To SRP</th>
                        <th>Date Changed</th>
                        <th></th>
                      </tr>  
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        <?php 
          }
        }
        ?>
        </div>
      </div>
    </section>

    <section id="products_section" class="content" style="display: none;">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 id="branch_name" class="box-title"></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <button id="export" type="button" class="btn btn-default pull-right" data-dateFrom="" data-dateTo="" data-priceMode="" data-branch="" style="display: none; margin-bottom: 10px;">
                <span class="fa fa-file-excel-o pull-left" style="font-size: 18px;"></span> Export As Excel
              </button>
              <table id="products_container" class="table table-bordered">
                <thead>
                  <tr>
                    <th>Barcode</th>
                    <th>Description</th>
                    <th>UOM</th>
                    <th>Price Mode</th>
                    <th>From SRP</th>
                    <th>To SRP</th>
                    <th>Date Changed</th>
                    <th></th>
                  </tr>  
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

  </div>
  <!-- /.content-wrapper -->
