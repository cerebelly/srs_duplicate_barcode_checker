  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Monitoring
        <small>Duplicate Barcode Checker</small>
      </h1>
      <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">Pace page</li>
      </ol> -->
    </section>


    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo (isset($branch_name) ? $branch_name: ''); ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tbody><tr>
                  <th>Product ID</th>
                  <th>Product Code</th>
                  <th>Barcode</th>
                  <th>Price Mode Code</th>
                  <th>Description</th>
                  <th>UOM</th>
                  <th>QTY</th>
                  <th>Markup</th>
                  <th>SRP</th>
                  <th>Last Date Modified</th>
                  <!-- <th style="width: 40px">Label</th> -->
                </tr>
                <?php
                  if (isset($row)) {
                    $span = 0;
                    $lastbarcode = null;
                    foreach ($row as $key => $value) {
                      if ($value['Barcode'] != $lastbarcode) {
                        $span = 0;
                        foreach ($row as $susi => $laman) {
                          if ($laman['Barcode'] == $value['Barcode']) {
                            $span += 1;
                          }
                        }
                      }
                      $rowspan = 'rowspan="'.$span.'"';
                      echo '<tr>';
                        echo '<td>'.$value['ProductID'].'</td>';
                        echo '<td>'.$value['ProductCode'].'</td>';
                        if ($value['Barcode'] != $lastbarcode) {
                          echo '<td '.$rowspan.' style="text-align: center; margin: 0 auto;">'.$value['Barcode'].'</td>';
                        }
                        echo '<td>'.$value['PriceModeCode'].'</td>';
                        echo '<td>'.$value['Description'].'</td>';
                        echo '<td>'.$value['uom'].'</td>';
                        echo '<td>'.$value['qty'].'</td>';
                        echo '<td>'.$value['markup'].'</td>';
                        echo '<td>'.$value['srp'].'</td>';
                        echo '<td>'.$value['LastDateModified'].'</td>';
                      echo '</tr>';
                      $lastbarcode = $value['Barcode'];
                    }
                  }
                ?>
              </tbody></table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                <!-- <li><a href="#">«</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">»</a></li> -->
              </ul>
            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
  </div>
  <!-- /.content-wrapper -->
