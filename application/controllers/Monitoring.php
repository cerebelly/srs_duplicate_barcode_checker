<?php 
defined('BASEPATH') OR exit('No direct script access allowed!');

class Monitoring extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Monitoring_model');
		$this->load->library('excel');

	}	

	public function index() {
		$this->body['title'] = 'Monitoring - Duplicate Barcode Checker';
		$this->data['branches'] = array(
				'ms_novaliches' => 'Novaliches',
				'ms_malabon' => 'Malabon',
				'ms_kusina_malabon' => 'Kusina Malabon',
				'ms_navotas' => 'Navotas',
				'ms_pavia' => 'Pavia',
				'ms_gagalangin' => 'Gagalangin',
				'ms_pateros' => 'Pateros',
				'ms_comembo' => 'Comembo',
				'ms_bagong_silang' => 'Bagong Silang',
				'ms_camarin' => 'Camarin',
				'ms_las_pinas' => 'Las Pinas',
				'ms_imus' => 'Imus',
				'ms_antipolo_1' => 'Antipolo 1',
				'ms_antipolo_2' => 'Antipolo 2',
				'ms_cainta_1' => 'Cainta 1',
				'ms_cainta_2' => 'Cainta 2',
				'ms_gt_deleon' => 'Gen. T. De Leon',
				'ms_punturin' => 'Punturin',
				'ms_san_pedro' => 'San Pedro',
				'ms_alaminos' =>'Alaminos'
			);

		// echo "<pre>";
		// print_r($this->data['branches']);
		// exit;

		if (sizeof($_POST) > 0) {
			// print_r($_POST);
			$barcode = $_POST['barcode'];
			$this->data['barcode'] = $barcode;
			$branch = $_POST['branch'];
			$this->data['branch'] = $branch;
			$this->data['branch_name'] = $this->data['branches'][$branch];
			$this->data['dbname'] = $branch;

			
			$this->data['row_main'] = $this->Monitoring_model->get_duplicates('main_'.$branch, $barcode);

			$this->data['row_branch'] = $this->Monitoring_model->get_duplicates($branch, $barcode);

			$this->load->view('common/header', $this->body);
			$this->load->view('common/sidebar');
			$this->load->view('monitoring', $this->data);
			$this->load->view('common/footer');

		}
		else {
			
			$this->load->view('common/header', $this->body);
			$this->load->view('common/sidebar');
			$this->load->view('monitoring', $this->data);
			$this->load->view('common/footer');
		}

	}

	public function get_excel($loc, $branch, $dbname, $barcode='') {
		$this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('SRS - Barcode Duplicate Checker');
        //set cell A1 content with some text
        $this->excel->getActiveSheet()->setCellValue('A1', $loc.'-'.$branch);
        $this->excel->getActiveSheet()->setCellValue('A4', 'Product ID');
        $this->excel->getActiveSheet()->setCellValue('B4', 'Product Code');
        $this->excel->getActiveSheet()->setCellValue('C4', 'Barcode');
        $this->excel->getActiveSheet()->setCellValue('D4', 'Price Mode Code');
        $this->excel->getActiveSheet()->setCellValue('E4', 'Description');
        $this->excel->getActiveSheet()->setCellValue('F4', 'UOM');
        $this->excel->getActiveSheet()->setCellValue('G4', 'QTY');
        $this->excel->getActiveSheet()->setCellValue('H4', 'Markup');
        $this->excel->getActiveSheet()->setCellValue('I4', 'SRP');
        $this->excel->getActiveSheet()->setCellValue('J4', 'Last Date Modified');

        //merge cell A1 until C1
        $this->excel->getActiveSheet()->mergeCells('A1:C1');
        //set aligment to center for that merged cell (A1 to C1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
        $this->excel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('#333');
		for($col = ord('A'); $col <= ord('C'); $col++){ //set column dimension $this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(true);
		         //change the font size
		        $this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
		         
		        $this->excel->getActiveSheet()->getStyle(chr($col))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		}
        //retrive contries table data
        load_db($dbname);
        $sql = 'SELECT ProductID, ProductCode, Barcode, PriceModeCode, Description,
        		uom, qty, markup, srp, LastDateModified
				FROM POS_Products
				WHERE Barcode IN (
				    SELECT Barcode
				    FROM POS_Products
				    GROUP BY Barcode
				    HAVING COUNT(ProductID) > 1
				    )';
		if (!empty($barcode)) {
			$sql .= ' AND Barcode = "'.$barcode.'"';
		}
		$sql .= ' ORDER BY Barcode';
     	$rs = $this->db->query($sql);
        $exceldata="";
		foreach ($rs->result_array() as $row){
		        $exceldata[] = $row;
		}
        //Fill data 
        $this->excel->getActiveSheet()->fromArray($exceldata, null, 'A5');
         
        $this->excel->getActiveSheet()->getStyle('A4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('B4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('C4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('D4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('E4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('F4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('I4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('J4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
         
        $filename= $loc.'-'.$branch.'-'.'duplicates.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
	}

}




?>