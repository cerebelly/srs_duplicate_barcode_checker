<?php
if (!defined('BASEPATH')) exit('No direct script access allowed!');

class Products_monitoring extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Products_monitoring_model');
		$this->load->library('excel');
		$this->data['branches'] = array(
				'ms_novaliches' => 'Novaliches',
				'ms_malabon' => 'Malabon',
				'ms_kusina_malabon' => 'Kusina Malabon',
				'ms_navotas' => 'Navotas',
				'ms_pavia' => 'Pavia',
				'ms_gagalangin' => 'Gagalangin',
				'ms_pateros' => 'Pateros',
				'ms_comembo' => 'Comembo',
				'ms_bagong_silang' => 'Bagong Silang',
				'ms_camarin' => 'Camarin',
				'ms_las_pinas' => 'Las Pinas',
				'ms_imus' => 'Imus',
				'ms_antipolo_1' => 'Antipolo 1',
				'ms_antipolo_2' => 'Antipolo 2',
				'ms_cainta_1' => 'Cainta 1',
				'ms_cainta_2' => 'Cainta 2',
				'ms_gt_deleon' => 'Gen. T. De Leon',
				'ms_punturin' => 'Punturin',
				'ms_san_pedro' => 'San Pedro',
				'ms_alaminos' => 'Alaminos',
				'ms_bagumbong' => 'Bagumbong'
			);
		$this->data['priceModes'] = array(
				'R' => 'Retail',
				'W' => 'Wholesale'
			);
	}

	public function price_change() {
		$this->body['title'] = 'Price Change Monitoring';
		$this->data['header'] = array(
			'title' => 'Products',
			'desc' => 'Price Change Monitoring'
		);
		$this->data['branches'] = $this->data['branches'];
		$this->data['priceModes'] = $this->data['priceModes'];

		$this->load->view('common/header', $this->body);
		$this->load->view('common/sidebar');
		$this->load->view('price_change', $this->data);
		$this->load->view('common/footer');
	}

	public function get_products() {
		if (sizeof($_POST) > 0) {
			// echo json_encode($_POST);
			$dateFrom = dateformat($this->input->post('dateFrom'), 'Y-m-d');
			$dateTo = dateformat($this->input->post('dateTo'), 'Y-m-d');
			$priceMode = $this->input->post('priceMode');
			$dbName = $this->input->post('branch');
			$products = $this->Products_monitoring_model->get_price_change($dateFrom, $dateTo, $priceMode, $dbName);
			if ($products) {
				$data = array(
					'status' => 'HAS_RESULT', 
					'branch_name' => $this->data['branches'][$dbName],
					'products' => $products
				);
				echo json_encode($data);
			}
			else {
				$data = array(
					'status' => 'NO_RESULT', 
					'branch_name' => $this->data['branches'][$dbName],
					'products' => $products
				);
				echo json_encode($data);
			}
		}
	}

	public function export_to_excel() {
		if (sizeof($_GET) > 0) {
			$dateFrom = dateformat($_GET['dateFrom'], 'Y-m-d');
			$dateTo = dateformat($_GET['dateTo'], 'Y-m-d');
			$priceMode = $_GET['priceMode'];
			$dbName = $_GET['branch'];
			$branchName = $this->data['branches'][$dbName];

			$this->excel->setActiveSheetIndex(0);
	        //name the worksheet
	        $this->excel->getActiveSheet()->setTitle('Price Change ('.$branchName.')');
	        //set cell A1 content with some text
	        $this->excel->getActiveSheet()->setCellValue('A1', $branchName .'- Price Change History ('.$dateFrom.' - '.$dateTo.')');
	        $this->excel->getActiveSheet()->setCellValue('A4', 'Barcode');
	        $this->excel->getActiveSheet()->setCellValue('B4', 'Description');
	        $this->excel->getActiveSheet()->setCellValue('C4', 'UOM');
	        $this->excel->getActiveSheet()->setCellValue('D4', 'Price Mode');
	        $this->excel->getActiveSheet()->setCellValue('E4', 'From SRP');
	        $this->excel->getActiveSheet()->setCellValue('F4', 'To SRP');
	        $this->excel->getActiveSheet()->setCellValue('G4', 'Date Changed');
	        $this->excel->getActiveSheet()->setCellValue('H4', '');

	        //merge cell A1 until C1
	        $this->excel->getActiveSheet()->mergeCells('A1:K1');
	        //set aligment to center for that merged cell (A1 to C1)
	        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	        //make the font become bold
	        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
	        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
	        $this->excel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('#333');
			for($col = ord('A'); $col <= ord('C'); $col++){ //set column dimension $this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(true);
			         //change the font size
			        $this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
			         
			        $this->excel->getActiveSheet()->getStyle(chr($col))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			}
	        //retrive contries table data
	        $rs = $this->Products_monitoring_model->get_price_change($dateFrom, $dateTo, $priceMode, $dbName);
	        $temp_rs = array();
	        if ($rs) {
	        	foreach ($rs as $key => $value) {
	        		$arr = array(
	        			'barcode' => $value['barcode'],
	        			'description' => $value['mdescription'],
	        			'uom' => $value['UOM'],
	        			'priceMode' => ($value['PriceModecode'] == 'R') ? 'RETAIL' : 'WHOLESALE',
	        			'fromSrp' => number_format($value['fromsrp'], 2),
	        			'toSrp' => number_format($value['tosrp'], 2),
	        			'datePosted' => dateformat($value['dateposted'], 'm/d/Y'),
	        			'increaseDecrease' => ($value['IncreaseDecrease'] == 1) ? 'Increased' : 'Decreased'
	        		);
	        		array_push($temp_rs, $arr);
	        	}
		        $exceldata="";
				foreach ($temp_rs as $row){
				        $exceldata[] = $row;
				}
		        //Fill data 
		        $this->excel->getActiveSheet()->fromArray($exceldata, null, 'A5');
		         
		        $this->excel->getActiveSheet()->getStyle('A4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		        $this->excel->getActiveSheet()->getStyle('B4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		        $this->excel->getActiveSheet()->getStyle('C4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		        $this->excel->getActiveSheet()->getStyle('D4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		        $this->excel->getActiveSheet()->getStyle('E4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		        $this->excel->getActiveSheet()->getStyle('F4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		        $this->excel->getActiveSheet()->getStyle('G4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		        $this->excel->getActiveSheet()->getStyle('H4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		         
		        $filename= 'priceChangeHistory_'.$dateFrom.'_'.$dateTo.'_'.$branchName.'.xls'; //save our workbook as this file name
		        header('Content-Type: application/vnd.ms-excel'); //mime type
		        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		        header('Cache-Control: max-age=0'); //no cache

		        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		        //if you want to save it as .XLSX Excel 2007 format
		        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		        //force user to download the Excel file without writing it to server's HD
		        $objWriter->save('php://output');
	        }

		}
	}

}