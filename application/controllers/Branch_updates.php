<?php
defined('BASEPATH') OR exit('No direct script access allowed!');

class Branch_updates extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Branch_updates_model');
		$this->data['branches'] = array(
				'ms_novaliches' => array(
					'branch_name' => 'Novaliches',
					'branch_code' => 'SRN',
					'main_conn' => '192.168.0.133',
					'branch_conn' => '192.168.0.179'
				),
				'ms_malabon' => array(
					'branch_name' => 'Malabon',
					'branch_code' => 'SRM',
					'main_conn' => '192.168.0.133',
					'branch_conn' => '192.168.1.100'
				),
				'ms_kusina_malabon' => array(
					'branch_name' => 'Kusina Malabon',
					'branch_code' => 'RML',
					'main_conn' => '192.168.0.133',
					'branch_conn' => '192.168.1.100'
				),
				'ms_navotas' => array(
					'branch_name' => 'Navotas',
					'branch_code' => 'SNV',
					'main_conn' => '192.168.0.133',
					'branch_conn' => '192.168.7.100'
				),
				'ms_pavia' => array(
					'branch_name' => 'Pavia/Tondo',
					'branch_code' => 'SRT',
					'main_conn' => '192.168.0.133',
					'branch_conn' => '192.168.3.100'
				),
				'ms_gagalangin' => array(
					'branch_name' => 'Gagalangin',
					'branch_code' => 'SRG',
					'main_conn' => '192.168.0.133',
					'branch_conn' => '192.168.4.100'
				),
				'ms_pateros' => array(
					'branch_name' => 'Pateros',
					'branch_code' => 'SPT',
					'main_conn' => '192.168.0.133',
					'branch_conn' => '192.168.16.100'
				),
				'ms_comembo' => array(
					'branch_name' => 'Comembo',
					'branch_code' => 'COM',
					'main_conn' => '192.168.0.133',
					'branch_conn' => '192.168.17.100'
				),
				'ms_bagong_silang' => array(
					'branch_name' => 'Bagong Silang',
					'branch_code' => 'SBS',
					'main_conn' => '192.168.0.133',
					'branch_conn' => '192.168.13.100'
				),
				'ms_camarin' => array(
					'branch_name' => 'Camarin',
					'branch_code' => 'SRC',
					'main_conn' => '192.168.0.133',
					'branch_conn' => '192.168.6.100'
				),
				'ms_las_pinas' => array(
					'branch_name' => 'Las Pinas',
					'branch_code' => 'LPN',
					'main_conn' => '192.168.0.133',
					'branch_conn' => '192.168.32.100'
				),
				'ms_imus' => array(
					'branch_name' => 'Imus',
					'branch_code' => 'SRI',
					'main_conn' => '192.168.0.133',
					'branch_conn' => '192.168.8.100'
				),
				'ms_antipolo_1' => array(
					'branch_name' => 'Antipolo 1',
					'branch_code' => 'SA1',
					'main_conn' => '192.168.0.133',
					'branch_conn' => '192.168.10.100'
				),
				'ms_antipolo_2' => array(
					'branch_name' => 'Antipolo 2',
					'branch_code' => 'SA2',
					'main_conn' => '192.168.0.133',
					'branch_conn' => '192.168.11.100'
				),
				'ms_cainta_1' => array(
					'branch_name' => 'Cainta 1',
					'branch_code' => 'SCA',
					'main_conn' => '192.168.0.133',
					'branch_conn' => '192.168.12.100'
				),
				'ms_cainta_2' => array(
					'branch_name' => 'Cainta 2',
					'branch_code' => 'SC2',
					'main_conn' => '192.168.0.133',
					'branch_conn' => '192.168.18.100'
				),
				'ms_gt_deleon' => array(
					'branch_name' => 'Valenzuela/Gen T.',
					'branch_code' => 'SRV',
					'main_conn' => '192.168.0.133',
					'branch_conn' => '192.168.14.100'
				),
				'ms_punturin' => array(
					'branch_name' => 'Punturin',
					'branch_code' => 'SPU',
					'main_conn' => '192.168.0.133',
					'branch_conn' => '192.168.15.100'
				),
				'ms_san_pedro' => array(
					'branch_name' => 'San Pedro',
					'branch_code' => 'PED',
					'main_conn' => '192.168.0.133',
					'branch_conn' => '192.168.19.100'
				),
				'ms_alaminos' => array(
					'branch_name' => 'Alaminos',
					'branch_code' => 'ALM',
					'main_conn' => '192.168.0.133',
					'branch_conn' => '192.168.20.100'
				),
				'ms_bagumbong' => array(
					'branch_name' => 'Bagumbong',
					'branch_code' => 'BGB',
					'main_conn' => '192.168.0.133',
					'branch_conn' => '192.168.21.100'
				)
			);
	}

	public function index() {
		$this->data['header'] = array(
			'title' => 'Branch Updates',
			'desc' => 'monitoring'
		);
		$this->data['branches'] = $this->data['branches'];

		// pre_r($this->data['branches']);

		$this->load->view('common/header');
		$this->load->view('common/sidebar');
		$this->load->view('branch_updates', $this->data);
		$this->load->view('common/footer');
	}

	public function get_main_updates() {
		if (sizeof($_POST) > 0) {
			$dateFrom = $_POST['dateFrom'];
			$dateTo = $_POST['dateTo'];
			$branchConn = $_POST['branchConn'];
			
			if ($conn = $this->ping_conn($this->data['branches'][$branchConn]['main_conn'], '1433')) {
				$res = $this->Branch_updates_model->get_main_updates('default', $this->data['branches'][$branchConn]['branch_code'], $dateFrom, $dateTo);
				$reponse = array('status' => 'HAS_RESULT', 'result' => $res);
				$reponse = json_encode($reponse);
				echo $reponse;
			}
			else {
				$reponse = array('status' => 'FAILED_TO_CONNECT');
				$reponse = json_encode($reponse);
				echo $reponse;
			}
		}
		else {
			$reponse = array('status' => 'INVALID_CREDENTIALS');
			$reponse = json_encode($reponse);
			echo $reponse;
		}
	}

	public function get_branch_updates() {
		if (sizeof($_POST) > 0) {
			$dateFrom = $_POST['dateFrom'];
			$dateTo = $_POST['dateTo'];
			$branchConn = $_POST['branchConn'];
			
			if ($conn = $this->ping_conn($this->data['branches'][$branchConn]['main_conn'], '1433')) {
				$res = $this->Branch_updates_model->get_main_updates($branchConn, $this->data['branches'][$branchConn]['branch_code'], $dateFrom, $dateTo);
				$reponse = array('status' => 'HAS_RESULT', 'result' => $res);
				$reponse = json_encode($reponse);
				echo $reponse;
			}
			else {
				$reponse = array('status' => 'FAILED_TO_CONNECT');
				$reponse = json_encode($reponse);
				echo $reponse;
			}
		}
		else {
			$reponse = array('status' => 'INVALID_CREDENTIALS');
			$reponse = json_encode($reponse);
			echo $reponse;
		}
	}

	public function ping_conn($host, $port=80) {
		$waitTimeoutInSeconds = 10;
		if ($fp = @fsockopen($host, $port, $errCode, $errStr, $waitTimeoutInSeconds)) {
			fclose($fp);
			return true;
		}
		else {
			return false;
		}
	}

}	