<?php defined('BASEPATH') OR exit('No direct script access allowed!');

/*if (function_exists("date_default_timezone_set") && function_exists("date_default_timezone_get")) 
	@date_default_timezone_set(@date_default_timezone_get());*/

define('TIMEZONE', 'Asia/Manila');

if (!function_exists('today')) 
{
	function today($format = 'Y-m-d H:i:s') 
	{
		$current_datetime = new DateTime(null, new DateTimeZone(TIMEZONE));
		return $current_datetime->format($format);
	}	
}

if (!function_exists('dateformat')) 
{
	function dateformat($date, $format='Y-m-d H:i:s')
	{
		$date = new DateTime($date, new DateTimeZone(TIMEZONE));
		return $date->format($format);
	}
}

if (!function_exists('is_date')) 
{
	function is_date($date) 
	{
		return (bool) strtotime($date); 
	}
}

if (!function_exists('add_days')) 
{
	function add_days($date, $days) // accept negative values as well 
	{
		list($day, $month, $year) = explode_date_to_dmy($date);
		$timet = mktime(0, 0, 0, $month, $day + $days, $year);
		$timet = date('Y-m-d', $timet);
		return dateformat($timet);
	}
}

if (!function_exists('explode_date_to_dmy')) 
{
	function explode_date_to_dmy($date_) 
	{
		$date = dateformat($date_);
		list($year, $month, $day) = explode('-', $date);
		return array($day, $month, $year);
	}
}

if (!function_exists('end_month')) 
{
	function end_month($date) 
	{
		list($day, $month, $year) = explode_date_to_dmy($date);
		$days_in_month = array(31, ((!($year % 4 ) && (($year % 100) || !($year % 400)))?29:28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
		return __date($year, $month, $days_in_month[$month - 1]); 
	}
}

if (!function_exists('__date')) 
{
	function __date($year, $month, $day) 
	{
		$sep = '-';
		$day = (int)$day;
		$month = (int)$month;

		if ($day < 10)
			$day = "0".$day;
		if ($month < 10)
			$month = "0".$month;

		return $year.$sep.$month.$sep.$day;
	}
}

if (!function_exists('date1_greater_date2')) 
{
	function date1_greater_date2($date1, $date2) 
	{
		/* returns 1 true if date1 is greater than date_ 2 */

		$date1 = dateformat($date1);
		$date2 = dateformat($date2);
		list($year1, $month1, $day1) = explode("-", $date1);
		list($year2, $month2, $day2) = explode("-", $date2);

		if ($year1 > $year2)
		{
			return 1;
		}
		elseif ($year1 == $year2)
		{
			if ($month1 > $month2)
			{
				return 1;
			}
			elseif ($month1 == $month2)
			{
				if ($day1 > $day2)
				{
					return 1;
				}
			}
		}
		return 0;
	}	
}


?>