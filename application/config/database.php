<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['dsn']      The full DSN string describe a connection to the database.
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database driver. e.g.: mysqli.
|			Currently supported:
|				 cubrid, ibase, mssql, mysql, mysqli, oci8,
|				 odbc, pdo, postgre, sqlite, sqlite3, sqlsrv
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Query Builder class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|				 NOTE: For MySQL and MySQLi databases, this setting is only used
| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
|				 (and in table creation queries made with DB Forge).
| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
| 				 can make your site vulnerable to SQL injection if you are using a
| 				 multi-byte character set and are running versions lower than these.
| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['encrypt']  Whether or not to use an encrypted connection.
|
|			'mysql' (deprecated), 'sqlsrv' and 'pdo/sqlsrv' drivers accept TRUE/FALSE
|			'mysqli' and 'pdo/mysql' drivers accept an array with the following options:
|
|				'ssl_key'    - Path to the private key file
|				'ssl_cert'   - Path to the public key certificate file
|				'ssl_ca'     - Path to the certificate authority file
|				'ssl_capath' - Path to a directory containing trusted CA certificats in PEM format
|				'ssl_cipher' - List of *allowed* ciphers to be used for the encryption, separated by colons (':')
|				'ssl_verify' - TRUE/FALSE; Whether verify the server certificate or not ('mysqli' only)
|
|	['compress'] Whether or not to use client compression (MySQL only)
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|	['ssl_options']	Used to set various SSL options that can be used when making SSL connections.
|	['failover'] array - A array with 0 or more data for connections if the main should fail.
|	['save_queries'] TRUE/FALSE - Whether to "save" all executed queries.
| 				NOTE: Disabling this will also effectively disable both
| 				$this->db->last_query() and profiling of DB queries.
| 				When you run a query, with this setting set to TRUE (default),
| 				CodeIgniter will store the SQL statement for debugging purposes.
| 				However, this may cause high memory usage, especially if you run
| 				a lot of SQL queries ... disable this to avoid that problem.
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $query_builder variables lets you determine whether or not to load
| the query builder class.
*/
$active_group = 'default';
$query_builder = TRUE;

$db['default'] = array(
	'dsn'	=> '',
	'hostname' => '192.168.0.133',
	'username' => 'markuser',
	'password' => 'tseug',
	'database' => 'NEWDATACENTER',
	'dbdriver' => 'mssql',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => FALSE,
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);

$main_ms_branches = array(
		# 1. Novaliches
		'main_ms_novaliches' => array(
				'hostname' => '192.168.0.133',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srsmnova'
			),
		# 2. Malabon
		'main_ms_malabon' => array(
				'hostname' => '192.168.0.133',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srsmmala'
			),
		# 2.1 SRS Kusina Malabon
		'main_ms_kusina_malabon' => array(
				'hostname' => '192.168.0.133',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'restoMMALA'
			),
		# 3. Navotas
		'main_ms_navotas' => array(
				'hostname' => '192.168.0.133',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srsmnavo'
			),
		# 4. Pavia
		'main_ms_pavia' => array(
				'hostname' => '192.168.0.133',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srsmton'
			),
		# 5. Gagalangin
		'main_ms_gagalangin' => array(
				'hostname' => '192.168.0.133',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srsmgal'
			),
		# 6. Pateros
		'main_ms_pateros' => array(
				'hostname' => '192.168.0.133',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srsmpat'
			),
		# 7. Comembo
		'main_ms_comembo' => array(
				'hostname' => '192.168.0.133',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srsmkum'
			),
		# 8. Bagong Silang
		'main_ms_bagong_silang' => array(
				'hostname' => '192.168.0.133',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srsmbsl'
			),
		# 9. Camarin
		'main_ms_camarin' => array(
				'hostname' => '192.168.0.133',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srsmcama'
			),
		# 10. Las Piñas
		'main_ms_las_pinas' => array(
				'hostname' => '192.168.0.133',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srsmpinas'
			), 
		# 11. Imus 
		'main_ms_imus' => array(
				'hostname' => '192.168.0.133',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srsmimu'
			),
		# 12. Antipolo 1
		'main_ms_antipolo_1' => array(
				'hostname' => '192.168.0.133',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srsmant1gf'
			),
		# 13. Antipolo 2
		'main_ms_antipolo_2' => array(
				'hostname' => '192.168.0.133',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srsmant2em'
			),
		# 14. Cainta 1
		'main_ms_cainta_1' => array(
				'hostname' => '192.168.0.133',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srsmcainta'
			),
		# 15. Cainta 2
		'main_ms_cainta_2' => array(
				'hostname' => '192.168.0.133',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srsmcainta2'
			),
		# 16. Gen. T. De Leon / Valenzuela
		'main_ms_gt_deleon' => array(
				'hostname' => '192.168.0.133',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srsmval'
			),
		# 17. Punturin
		'main_ms_punturin' => array(
				'hostname' => '192.168.0.133',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srsmpun'
			),
		# 18. San Pedro
		'main_ms_san_pedro' => array(
				'hostname' => '192.168.0.133',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srsmpedro'
			),
		# 19. Alaminos
		'main_ms_alaminos' => array(
				'hostname' => '192.168.0.133',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srsmalam'
			),
		# 20. Bagumbong
		'main_ms_bagumbong' => array(
				'hostname' => '192.168.0.133',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srsmbag'
			)
	);


foreach ($main_ms_branches as $name => $value) {
	// echo $name."</br>";
	$db[$name] = array(
		'dsn'	=> '', // Driver={SQL Server};Server=192.168.3.100;Database=srpos;
		'hostname' => $value['hostname'],
		'username' => $value['username'],
		'password' => $value['password'],
		'database' => $value['database'],
		'dbdriver' => 'mssql',
		'dbprefix' => '',
		'pconnect' => FALSE,
		'db_debug' => TRUE,
		'cache_on' => FALSE,
		'cachedir' => '',
		'char_set' => FALSE,
		'dbcollat' => 'utf8_general_ci',
		'swap_pre' => '',
		'encrypt' => FALSE,
		'compress' => FALSE,
		'stricton' => FALSE,
		'failover' => array(),
		'save_queries' => TRUE
	);

}


$ms_branches = array(
		# 1. Novaliches
		'ms_novaliches' => array(
				'hostname' => '192.168.0.179',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srspos'
			),
		# 2. Malabon
		'ms_malabon' => array(
				'hostname' => '192.168.1.100',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srpos'
			),
		# 2.1 SRS Kusina Malabon
		'ms_kusina_malabon' => array(
				'hostname' => '192.168.1.100',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'RestoBMALA'
			),
		# 3. Navotas
		'ms_navotas' => array(
				'hostname' => '192.168.7.100',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srnav'
			),
		# 4. Pavia
		'ms_pavia' => array(
				'hostname' => '192.168.3.100',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srpos'
			),
		# 5. Gagalangin
		'ms_gagalangin' => array(
				'hostname' => '192.168.4.100',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srgala'
			),
		# 6. Pateros
		'ms_pateros' => array(
				'hostname' => '192.168.16.100',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srspat'
			),
		# 7. Comembo
		'ms_comembo' => array(
				'hostname' => '192.168.17.100',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'skum'
			),
		# 8. Bagong Silang
		'ms_bagong_silang' => array(
				'hostname' => '192.168.13.100',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srsbsl'
			),
		# 9. Camarin
		'ms_camarin' => array(
				'hostname' => '192.168.6.100',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srcama'
			),
		# 10. Las Piñas
		'ms_las_pinas' => array(
				'hostname' => '192.168.32.100',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srspinas'
			), 
		# 11. Imus 
		'ms_imus' => array(
				'hostname' => '192.168.8.100',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srimu'
			),
		# 12. Antipolo 1
		'ms_antipolo_1' => array(
				'hostname' => '192.168.10.100',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srsant1gf'
			),
		# 13. Antipolo 2
		'ms_antipolo_2' => array(
				'hostname' => '192.168.11.100',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srsmant2em'
			),
		# 14. Cainta 1
		'ms_cainta_1' => array(
				'hostname' => '192.168.12.100',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srsmcainta'
			),
		# 15. Cainta 2
		'ms_cainta_2' => array(
				'hostname' => '192.168.18.100',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srscainta2'
			),
		# 16. Gen. T. De Leon / Valenzuela
		'ms_gt_deleon' => array(
				'hostname' => '192.168.14.100',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srsval'
			),
		# 17. Punturin
		'ms_punturin' => array(
				'hostname' => '192.168.15.100',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srspun'
			),
		# 18. San Pedro
		'ms_san_pedro' => array(
				'hostname' => '192.168.19.100',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srspedro'
			),
		# 19. Alaminos
		'ms_alaminos' => array(
				'hostname' => '192.168.20.100',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srsalam'
			), 
		# 20. Bagumbong
		'ms_bagumbong' => array(
				'hostname' => '192.168.21.100',
				'username' => 'markuser',
				'password' => 'tseug',
				'database' => 'srsbag'
			)
	);


foreach ($ms_branches as $name => $value) {
	// echo $name."</br>";
	$db[$name] = array(
		'dsn'	=> '', // Driver={SQL Server};Server=192.168.3.100;Database=srpos;
		'hostname' => $value['hostname'],
		'username' => $value['username'],
		'password' => $value['password'],
		'database' => $value['database'],
		'dbdriver' => 'mssql',
		'dbprefix' => '',
		'pconnect' => FALSE,
		'db_debug' => TRUE,
		'cache_on' => FALSE,
		'cachedir' => '',
		'char_set' => FALSE,
		'dbcollat' => 'utf8_general_ci',
		'swap_pre' => '',
		'encrypt' => FALSE,
		'compress' => FALSE,
		'stricton' => FALSE,
		'failover' => array(),
		'save_queries' => TRUE
	);

}